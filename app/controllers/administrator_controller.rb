class AdministratorController < ApplicationController
  layout "admin"
  before_filter :admin_user, except: :index

  def index
  end

  def users
    @users = User.all
  end

  def orders
  end

  def routes
  end

  def postal_errors
    @postal_errors = PostalError.all
  end

  private
    def admin_user
      redirect_to(root_path) unless current_user.admin?
    end
end
