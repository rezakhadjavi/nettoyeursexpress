class ServicePointsController < ApplicationController
  def create
    @service_point = current_user.service_points.build(params[:service_point])
    @token = Token.new
    @postal_code = params[:service_point][:postal_code]
    @name = current_user.profile.name
    @email = current_user.email
    @cash = current_user.billings.find_by_method("cash")

    if @service_point.save
      respond_to do |format|
        format.html { redirect_to root_path, :notice => "Profile created!"}
        format.js 
      end
    else
      PostalError.create(postal_code: @postal_code, name: @name, email: @email)
      respond_to do |format|
        format.js { render :action => "fail" }
      end  
    end

  end

  def update
  end

  def destroy
  end

  def fail
  end
end
