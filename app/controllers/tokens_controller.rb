class TokensController < ApplicationController
  def create
    @token = current_user.tokens.build(params[:token])
    

    if @token.save
      @primary = current_user.billings.find_by_primary(true)
      @secondary = current_user.billings.where(:primary => false).where("method != 'cash'")
      @cash = current_user.billings.find_by_method("cash")
      
      respond_to do |format|
        format.html { redirect_to root_path, :notice => "Card Added!"}
        format.js 
      end
    else
      respond_to do |format|
        format.js { render :action => "fail" }
      end  
    end

  end

  def update
  end

  def fail
    
  end

  def update_exp
    

    @new_month = params[:exp_m]
    @year = params[:exp_y]
    @new_year = @year[-2, 2]

    @token_id = params[:update_token_id]

    @token = Token.find(@token_id.to_i)

    @token.update_attributes(:exp_m => @new_month, :exp_y => @new_year)

    @billing = Billing.find_by_token_id(@token_id.to_i)

    if @billing.update_attribute(:method, "#{@token.last_four} (Exp: #{@new_month}/#{@new_year})")
      @primary = current_user.billings.find_by_primary(true)
      @secondary = current_user.billings.where(:primary => false).where("method != 'cash'")
      @cash = current_user.billings.find_by_method("cash")

      respond_to do |format|
        format.js 
      end
    end

  end
end
