class OrdersController < ApplicationController

  def create
  end

  def update
  end

  def step2
    @p_date = params[:p_date]
    @p_time = params[:p_time]

    @d_time = @p_time

    @p_address = "#{current_user.service_points.first.address} #{current_user.service_points.first.apt}"
    @p_address_full = "#{current_user.service_points.first.address} #{current_user.service_points.first.apt},  #{current_user.service_points.first.postal_code}"
    @p_postal = current_user.service_points.first.postal_code

    @d_address = "#{current_user.service_points.first.address} #{current_user.service_points.first.apt}"
    @d_address_full = "#{current_user.service_points.first.address} #{current_user.service_points.first.apt},  #{current_user.service_points.first.postal_code}"
    @d_postal = current_user.service_points.first.postal_code

    @billing = current_user.profile.billing
    @user_id = "#{current_user.id}"

    respond_to do |format| 
      format.html { redirect_to account_path }
      format.js { render :action => "step2", :locals => { :p_date => @p_date, :p_time => @p_time } }
    end
  end

  def submit_order

    @p_date = params[:p_date]
    @p_time = params[:p_time]
    @p_address = params[:p_address]
    @p_postal = params[:p_postal]
    @p_notes = params[:p_notes]
    @d_date = params[:d_date]
    @d_time = params[:d_time]
    @d_address = params[:d_address]
    @d_postal = params[:d_postal]
    @d_notes = params[:d_notes]
    @service = params[:service]
    @c_notes = params[:c_notes]
    @speed = params[:speed]
    @promo_code = params[:promo_code]
    @user_id = params[:user_id]


    respond_to do |format| 

      if Order.create(
        p_date: @p_date, 
        p_time: @p_time, 
        p_address: @p_address, 
        p_postal: @p_postal,
        p_notes: @p_notes,
        d_date: @d_date, 
        d_time: @d_time, 
        d_address: @d_address, 
        d_postal: @d_postal,
        d_notes: @d_notes,
        service: @service,
        c_notes: @c_notes,
        speed: @speed,
        promo_code: @promo_code,
        user_id: @user_id
        )

        format.html { redirect_to account_path }
        format.js { render :action => "order_success" }
      
      else
      
        flash[:notice] = "error"
      
      end
    
    end
    
  end

end
