class ProfilesController < ApplicationController
  def create
    @profile = current_user.build_profile(params[:profile])
    @service_point = ServicePoint.new
    respond_to do |format|
      if @profile.save

        #Create cash as primary billing option here
        Billing.create(method: "cash", primary: true, user_id: current_user.id)
        
        format.html { redirect_to root_path, :notice => "Profile created!"}
        format.js
        
      else
        flash[:notice] = "error"
      end
    end
  end

  def update
    @profile = current_user.profile
    @billing = params[:profile][:billing]
    respond_to do |format|
      if @profile.update_attribute(:billing, @billing)
         format.html { redirect_to account_path, :notice => "Updated!" }
          format.js
      else
        flash[:notice] = "error"
      end
    end
  end

  def edit_profile_ajax
    @profile = Profile.new
    @current_user = current_user

    respond_to do |format| 
      format.html {redirect_to account_path, :notice => "something went wrong" }
      format.js
    end
  end



  def add_cc
    @profile = current_user.profile
    @credit = params[:billing]
    
    respond_to do |format|
      if @profile.update_attribute(:billing, @credit)
        format.html { redirect_to account_path, :notice => "Updated!" }
        format.js { render :action => "cc_success" }
      else
        flash[:notice] = "error"
      end
    end
  end

  def complete
    @complete = params[:profile_complete]
    if current_user.update_attribute(:profile_complete, true)
      redirect_to account_path
    end
  end
end
