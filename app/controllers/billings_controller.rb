class BillingsController < ApplicationController

  #Remember that each user is assigned "cash" as default billing method in ProfilesController

  def make_primary
    current_user.update_attribute(:profile_complete, true)
    
    @billing_id = params[:billing_id]

    if make_primary_loop(@billing_id)
      @primary = current_user.billings.find_by_primary(true)
      @secondary = current_user.billings.where(:primary => false).where("method != 'cash'")
      @cash = current_user.billings.find_by_method("cash")
      respond_to do |format|
        format.js 
      end 
    end

    
    
  end

  def remove
    @billing_id = params[:billing_id]

    if Billing.find(@billing_id).destroy
      @primary = current_user.billings.find_by_primary(true)
      @secondary = current_user.billings.where(:primary => false).where("method != 'cash'")
      @cash = current_user.billings.find_by_method("cash")
      respond_to do |format|
        format.js 
      end 
    end
  end

  def remove_primary
    @billing_id = params[:billing_id]
    @secondary = current_user.billings.where(:primary => false).where("method != 'cash'")
    @cash = current_user.billings.find_by_method("cash")

    if Billing.find(@billing_id).destroy
      if @secondary.any?
        @nextup = @secondary.first
        @nextup.update_attribute(:primary, true)
        @primary = @nextup
      else
        @cash.update_attribute(:primary, true)
        @primary = @cash
      end
      
      respond_to do |format|
        format.js 
      end 
    end
  end

end
