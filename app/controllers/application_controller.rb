class ApplicationController < ActionController::Base
  protect_from_forgery

  def after_sign_in_path_for(resource)
    account_path
  end

  def make_primary_loop(billing_id)

    current_user.billings.each do |billing|
      if billing.id == billing_id.to_i
        billing.update_attribute(:primary, true)
      else
        billing.update_attribute(:primary, false)
      end
    end
    
  end


end
