class PostalErrorsController < ApplicationController
  def index
    @postal_errors = PostalError.all
  end

  def new
    @postal_error = PostalError.new
  end

  def create
    @postal_error = PostalError.new(params[:postal_error])
    if @postal_error.save
      flash[:alert] = "We're sorry, you are outside of the zone"
      redirect_to action: 'zone_error_message'
    else
      flash[:notice] = "You're in!"
      redirect_to new_user_registration_path
    end
  end

  def destroy
    @postal = PostalError.find(params[:id])

    @postal.destroy

    redirect_to postal_errors_path
  end

  def zone_error_message
    
  end
end
