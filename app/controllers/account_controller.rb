class AccountController < ApplicationController
  before_filter :auth_user
  before_filter :check_welcome, except: :welcome
  before_filter :block_welcome, only: :welcome

  def welcome
    @profile = Profile.new
    @token = Token.new
    @service_point = ServicePoint.new
    @cash = current_user.billings.find_by_method("cash")
    set_step

  end
  
  def profile
    @profile = Profile.new
    @service_point = ServicePoint.new
  end

  def new_order
    respond_to do |format|
      format.html
      format.js { render :action => "new_order" }
    end
  end
  
  def orders
  end

  def billing
    @token = Token.new
    @primary = current_user.billings.find_by_primary(true)
    @secondary = current_user.billings.where(:primary => false).where("method != 'cash'")
    @cash = current_user.billings.find_by_method("cash")
  end

  def service_points
    
  end

  def tell_a_friend
  end

  

  private
    def set_step
      if current_user.profile.nil?
        @step1 = true
        @percentage = "50%"
      elsif current_user.service_points.empty?
        @step2 = true
        @percentage = "75%"
      else 
        @step3 = true
        @percentage = "95%"
      end
    end
    def auth_user
      redirect_to new_user_session_path unless user_signed_in?
    end
    def check_welcome
      unless current_user.profile_complete
        redirect_to welcome_path
      end
    end
    def block_welcome
      if current_user.profile_complete
        redirect_to account_path 
      end
    end
end
