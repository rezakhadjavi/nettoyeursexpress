class ZonesController < ApplicationController

  def index
    @zones = Zone.all
  end

  def show
    @zone = Zones.find(params[:id])
  end

  def new
    @zone = Zone.new
  end

  def create
    @zone = Zone.new(params[:zone])
    if @zone.save
      redirect_to @zone, notice: "New Zone Added" 
    else
      render "new"
    end
  end

  def destroy
    @zone = Zone.find(params[:id])
    @zone.destroy
    redirect_to zones_path
  end

  
end
