class StaticPagesController < ApplicationController
  def home
  end

  def services
  end

  def prices
  end

  def delivery
  end
end
