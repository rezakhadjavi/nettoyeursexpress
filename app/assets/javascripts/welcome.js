//Welcome
$('.show-reg-phone').on('click', function(){

  $('#profile_cell').parsley('removeConstraint', 'required');

  $('.cell-label').text("Cell Phone")

  $('#profile_phone').parsley('addConstraint', { required: "true" });

  $('.reg-phone').slideDown();

  $(this).slideUp(); 

});

$('#new_profile').parsley( {

  validators: {

    remote: null,

    type: null

  }

});

$('#new_service_point').parsley( {

  validators: {

    remote: null,

    type: null

  }

});

$('#new_token').parsley( {

  validators: {

    remote: null,

    type: null

  }

});


//billing stuff
//If edit, also edit service_points
$('.cc-button').on("click", function(){

  $('.token-form-wrapper').slideDown();

  $(this).removeClass('secondary');

  $('.cash-button').addClass('secondary');

});

$('.cash-button').on("click", function(){

  $('.token-form-wrapper').slideUp();
  
});

