$(document).ready(function(){

  $('.p_date').val("");

  $('.p_time').val("");

  $('.go-to-step2').attr('disabled', 'disabled');

})

$('.new-order-btn').on('click', function(){

  $('.order-box-bg-full').css('marginTop', '8px');

  $(this).fadeOut();

})

// STEP 1

// Actions related to P-Date button click 
$('.p-date-button').on('click', function(){
  var times = $(this).data("times"),
      size = document.documentElement.clientWidth,
      date = $(this).data("date");

  //update actives, update hidden input, toggle disabled button
  $('.p-date-button').removeClass('active');

  $(this).addClass('active');

  $('.p_date').val(date);

  $('.p-time-btn').removeClass('active');

  $('.p_time').val("");

  $('.go-to-step2').attr('disabled', 'disabled');


  //Assign the approprate times based on data-times logic  
  if ( times == "alltimes" ) {
    $('.morning').show().prependTo('.p-time-list');
    $('.evening').show();
    $('.afternoon').hide();
  }
  else if ( times == "evening" ) {
    $('.evening').show().prependTo('.p-time-list');
    $('.morning').hide();
    $('.afternoon').hide();
  }
  else if ( times == "afternoon" ) {
    $('.morning').hide();
    $('.evening').hide();
    $('.afternoon').show().prependTo('.p-time-list');
  }

  // Perform different slidedown for time windows for mobile screen
  if ( size <= 600 ) {
    if ( $('.p-time-wrapper' ).css('display') == 'none' ) {
      $('.p-time-wrapper').insertAfter($(this)).slideDown();
    }
    else {
      $('.p-time-wrapper').slideUp('fast').insertAfter($(this)).slideDown();
    }
    $('body').scrollTo( $(this), { duration:500} );
  }
  else {
    $('.p-time-wrapper').slideDown('fast');
  }

});

$('.p-time-btn').on('click', function(){

  var time = $(this).text();
  $('.go-to-step2').removeAttr('disabled');
  $('.p_time').val(time);
  $('.p-time-btn').removeClass('active');
  $(this).addClass('active');

})

// STEP 2
