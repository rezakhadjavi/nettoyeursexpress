//Global Functions
var successMsg = (function(message) {
  
  if ( size <= 400 ){
    $('body').scrollTo( $('.billing-table-wrapper '), { duration: 500} );
  } 
  else{
    $('body').scrollTo( $('.main-header'), {duration: 500} );
  }
  
  $('.notification-bar').text(message).addClass("success").slideDown('fast').delay(1500).slideUp();


});

var errorMsg = (function(message) {
  
  $('.notification-bar').text(message).addClass("alert").slideDown('fast').delay(1500).slideUp();

});




//Update EXP Date
var updateBtnClickEvent = (function(){
  $('.update-exp-btn').on('click', function(){
  var token_id = $(this).data('token-id'),
      last_four_full = $(this).data('last-four'),
      last_four = last_four_full.substring(0, last_four_full.length - 13),
      card_class = $(this).data('card-type');

  $('#update-cc-modal').foundation('reveal', 'open');

  $('.update-last-four').text(last_four);

  $('.update-method-title').addClass(card_class);

  $('.update-token-id').val(token_id)

  })
});

updateBtnClickEvent();



// Parsley
$('#new_token').parsley( {

  validators: {

    remote: null,

    type: null

  }

});
$('#update-cc-form').parsley( {

  validators: {

    remote: null,

    type: null

  }

});

// ICON-AJAX BINDING  

var bindIconAjaxLoading = (function(){
  $('.make-primary').bind('ajax:before', function(){

  $(this).find('.icon-star').addClass('icon-spin icon-spinner ')

  });

  $('.remove-billing').bind('ajax:before', function(){

  $(this).find('.icon-trash').addClass('icon-spin icon-spinner ')

  });
});

bindIconAjaxLoading();