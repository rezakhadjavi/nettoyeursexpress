//WELCOME FORMS

$('#new_profile').bind('ajax:before', function(){

  $('.welcome-loading').fadeIn('fast');

});

$('#new_profile').bind('ajax:success', function(){

  $('.welcome-loading').fadeOut();

});

$('#new_service_point').bind('ajax:before', function(){

  $('.welcome-loading').fadeIn('fast');

});

$('#new_service_point').bind('ajax:success', function(){

  $('.welcome-loading').fadeOut();

});


$('#new_token').bind('ajax:before', function(){

  $('.token-loading').fadeIn('fast');

});

$('#new_token').bind('ajax:success', function(){

  $('.token-loading').fadeOut();

});

$('#update-cc-form').bind('ajax:before', function(){

  $('.token-loading').fadeIn('fast');

});

$('#update-cc-form').bind('ajax:success', function(){

  $('.token-loading').fadeOut();

});

//ORDER FORMS

$('.step1-form').bind('ajax:before', function(){

  $('.welcome-loading').fadeIn('fast');

  // Fade out of this loader is done in step2.js.erb

});


