class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :profile_complete
  # attr_accessible :title, :body
  has_one :profile
  has_many :service_points, dependent: :destroy
  has_many :tokens
  has_many :orders
  has_many :billings
end
