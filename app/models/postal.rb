class Postal < ActiveRecord::Base
  attr_accessible :postal_code, :zone_id

  belongs_to :zone
end
