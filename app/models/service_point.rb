class ServicePoint < ActiveRecord::Base
  attr_accessible :address, :apt, :door_code, :instructions, :postal_code, :title, :user_id, :zone_id
  belongs_to :zone
  belongs_to :user

  before_save :check_postal

  def check_postal
    first_three = self.postal_code[0..2]
    first_three.downcase!

    postal = Postal.find_by_postal_code(first_three)

    if postal
      self.zone_id = postal.zone_id
    else
      return false
    end
  end


end