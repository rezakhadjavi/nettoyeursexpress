class Billing < ActiveRecord::Base
  attr_accessible :method, :primary, :token_id, :user_id
  belongs_to :user
  belongs_to :token
end
