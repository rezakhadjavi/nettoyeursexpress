class Token < ActiveRecord::Base
  attr_accessible :exp_m, :exp_y, :last_four, :token, :user_id
  belongs_to :user
  before_create :tokenize_cc
  before_create :hide_number
  after_create :make_primary
  after_create :add_to_billing

  def tokenize_cc
    self.token = SecureRandom.hex(n=4)
  end

  def hide_number
    card = self.last_four
    last = card[-4,4]
    first = card[0.1]

    self.last_four = "#{first}xxxxxxxxxxx#{last}"
  end

  def make_primary
    user = User.find(user_id)

    user.billings.each do |billing|
      billing.update_attribute(:primary, false)
    end
  end

  def add_to_billing
    year = self.exp_y[-2,2]
    token_id = self.id
    method = "#{self.last_four} (Exp: #{self.exp_m}/#{year})"
    user_id = self.user_id

    Billing.create(method: method, primary: true, user_id: user_id, token_id: token_id)
  end

end
