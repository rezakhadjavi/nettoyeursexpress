class PostalError < ActiveRecord::Base
  attr_accessible :postal_code, :name, :email
end
