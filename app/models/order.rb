class Order < ActiveRecord::Base
  attr_accessible :c_notes, :billing, :d_address, :d_date, :d_notes, :d_postal, :d_time, :invoice_id, :p_address, :p_date, :p_notes, :p_postal, :p_time, :promo_code, :service, :speed, :status, :total, :user_id
  belongs_to :user
end
