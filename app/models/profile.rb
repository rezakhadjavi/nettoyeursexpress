class Profile < ActiveRecord::Base
  attr_accessible :ext, :name, :phone, :cell, :user_id, :billing
  belongs_to :user
  before_save do |profile| 

    cell = profile.cell
    phone = profile.phone

    profile.cell = cell.gsub(/[^0-9]/i, '') 
    profile.phone = phone.gsub(/[^0-9]/i, '')
  end

end
