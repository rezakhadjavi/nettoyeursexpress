class Zone < ActiveRecord::Base
  attr_accessible :name
  has_many :service_points
  has_many :postals
end
