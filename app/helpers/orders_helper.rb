module OrdersHelper

  def dates_this_week
    d = Date.today

    if time >= "15"
      d+=1
    end
    #this simply creates an array length of 6 then joins them as string then creates a new array (to avoid this_week[n] = nil if "Sun")
    (d...d+6).map{|a|
      "#{a}+" unless a.strftime("%a") == "Sun"
    }.join.split("+")
  end

  def to_my_date(date)
    "#{ date.strftime("%a")}, #{date.strftime("%d")} #{date.strftime("%b") }"
  end

  def format_week(date)
    my_date = Date.parse(date)
    to_my_date(my_date)
  end

  def time
    t = Time.now
    "#{t.strftime("%H")}"
  end

  def hours
     form_tag({controller: "orders", action: "step2"}, method: "put", remote: true) do |f|
    
      hidden_field_tag :p_date, ""
      hidden_field_tag :p_time, ""

     submit_tag("Next", :class => "button")

     end
  end 

  def afternoon_not_sunday
    d = Date.today
    if d.strftime("%a") == "Sun"
      true
    elsif time <= "08"
      true
    else
      false
    end
  end

  # Pickup Helpers 
  def pickup_times(day)
    day_date = Date.parse(day)
    day_name = day_date.strftime("%a")

    if day == dates_this_week[0]
      if afternoon_not_sunday
        "alltimes"
      else
        "evening"
      end
    elsif day_name == "Sat"
      "afternoon"
    else
      "alltimes"
    end
  end

  # Delivery Helpers
  def reg_del_date(pickup)
    p_date = Date.parse(pickup)
    d_date = p_date + 2
    if p_date.strftime("%a") == "Sat"
      d_date+=1
    elsif d_date.strftime("%a") == "Sun"
      d_date+=1
      return d_date
    else
      return d_date
    end
  end

  def exp_del_date(pickup)
    p_date = Date.parse(pickup)
    d_date = p_date + 1
    if p_date.strftime("%a") == "Sat"
      d_date+=1
    elsif d_date.strftime("%a") == "Sun"
      d_date+=1
      return d_date
    else
      return d_date
    end
  end

  def del_times(p_date, p_time)
    day_date = Date.parse(p_date)
    day_name = day_date.strftime("%a")
    
    if day_name == "Sat"
      return "18:00 - 21:00"
    elsif day_name == "Thu"
      return "15:00 - 18:00"
    else
      return p_time
    end
  end

  def exp_del_times(p_date, p_time)
    day_date = Date.parse(p_date)
    day_name = day_date.strftime("%a")
    
    if p_time == "19:00 - 21:00" && day_name != "Fri"
      return p_time
    elsif day_name == "Fri"
      return "15:00 - 18:00"
    else
      return "18:00 - 21:00"
    end
  end

  

  

end
