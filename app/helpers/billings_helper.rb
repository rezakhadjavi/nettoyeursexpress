module BillingsHelper

  def cc_class(last_four)
    
    first = last_four[0.1]

    if first == "4"
    
      "cc-icons-visa"
    
    elsif first == "5"
    
      "cc-icons-mastercard"
    
    elsif first == "3"
      
      "cc-icons-amex"

    end

  end


end
