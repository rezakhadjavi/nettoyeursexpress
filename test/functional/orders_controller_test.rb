require 'test_helper'

class OrdersControllerTest < ActionController::TestCase
  test "should get create" do
    get :create
    assert_response :success
  end

  test "should get update" do
    get :update
    assert_response :success
  end

  test "should get step2" do
    get :step2
    assert_response :success
  end

end
