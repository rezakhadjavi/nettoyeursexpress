require 'test_helper'

class AdministratorControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get users" do
    get :users
    assert_response :success
  end

  test "should get orders" do
    get :orders
    assert_response :success
  end

  test "should get routes" do
    get :routes
    assert_response :success
  end

  test "should get postal_errors" do
    get :postal_errors
    assert_response :success
  end

end
