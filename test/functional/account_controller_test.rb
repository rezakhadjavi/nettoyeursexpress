require 'test_helper'

class AccountControllerTest < ActionController::TestCase
  test "should get profile" do
    get :profile
    assert_response :success
  end

  test "should get orders" do
    get :orders
    assert_response :success
  end

  test "should get billing" do
    get :billing
    assert_response :success
  end

  test "should get tell_a_friend" do
    get :tell_a_friend
    assert_response :success
  end

end
