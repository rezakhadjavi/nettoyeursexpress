require 'test_helper'

class BillingsControllerTest < ActionController::TestCase
  test "should get make_primary" do
    get :make_primary
    assert_response :success
  end

  test "should get remove" do
    get :remove
    assert_response :success
  end

end
