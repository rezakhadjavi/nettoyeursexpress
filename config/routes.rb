NettoyeursExpress::Application.routes.draw do
  
  root to: 'static_pages#home'
  #StaticPages
  match '/services', to: 'static_pages#services'
  match '/prices', to: 'static_pages#prices'
  match '/delivery', to: 'static_pages#delivery'

  #UsersController
  devise_for :users
  #AccountController
  match '/account', to: 'account#dashboard'
  match '/account/orders', to: 'account#orders'
  
  match '/neworder', to: 'account#new_order'
  match '/account/billing', to: 'account#billing'
  match '/account/service-points', to: 'account#service_points'
  match '/account/tell-a-friend', to: 'account#tell_a_friend'
  match '/edit_profile_form', to: 'profiles#edit_profile_ajax'
  match '/add_cash', to: 'profiles#add_cash'
  match '/add_cc', to: 'profiles#add_cc'
  match '/complete', to: 'profiles#complete'
  match '/welcome', to: 'account#welcome'
  #Administrator
  match '/administrator', to: 'administrator#index'
  match '/administrator/users', to: 'administrator#users'
  match '/administrator/orders', to: 'administrator#orders'
  match '/administrator/routes', to: 'administrator#routes'
  match '/administrator/postal_errors', to: 'administrator#postal_errors'

  #Orders
  match '/step2', to: 'orders#step2'
  match '/submit_order', to: 'orders#submit_order'

  #Billing
  match '/make_primary', to: 'billings#make_primary'
  match '/remove_primary', to: 'billings#remove_primary'
  match '/remove', to: 'billings#remove'

  #Tokens
  match '/update_exp', to: 'tokens#update_exp'
  
  #Resources
  resources :zones
  resources :postals
  resources :user do
    resource :profiles
    resources :service_points, only: [:create, :update, :destroy]
    resources :tokens, only: [:create, :update, :destroy]
    resources :orders, only: [:create, :update, :destroy]
  end

  #MISC
  match '/postal_errors/zone_error_message', :controller => 'postal_errors', :action => 'zone_error_message'
  get "postal_errors/index"
  get "postal_errors/new"
  get "postal_errors/create"
  get "postal_errors/destroy"
  resources :postal_errors
  
  


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
