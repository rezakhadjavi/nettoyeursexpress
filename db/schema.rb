# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130723150825) do

  create_table "billings", :force => true do |t|
    t.string   "method"
    t.boolean  "primary"
    t.integer  "user_id"
    t.integer  "token_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "orders", :force => true do |t|
    t.date     "p_date"
    t.string   "p_time"
    t.date     "d_date"
    t.string   "d_time"
    t.string   "p_address"
    t.string   "d_address"
    t.string   "p_postal"
    t.string   "d_postal"
    t.string   "p_notes"
    t.string   "d_notes"
    t.string   "service"
    t.string   "speed"
    t.string   "promo_code"
    t.string   "billing"
    t.string   "status"
    t.string   "total"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "c_notes"
  end

  create_table "postal_errors", :force => true do |t|
    t.string   "postal_code"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "email"
    t.string   "name"
  end

  create_table "postals", :force => true do |t|
    t.string   "postal_code"
    t.string   "zone_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "postals", ["zone_id"], :name => "index_postals_on_zone_id"

  create_table "profiles", :force => true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "ext"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "cell"
    t.string   "billing"
  end

  add_index "profiles", ["cell"], :name => "index_profiles_on_cell"
  add_index "profiles", ["phone", "user_id"], :name => "index_profiles_on_phone_and_user_id"

  create_table "service_points", :force => true do |t|
    t.string   "address"
    t.string   "apt"
    t.string   "door_code"
    t.string   "postal_code"
    t.string   "title"
    t.string   "instructions"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "zone_id"
  end

  add_index "service_points", ["user_id"], :name => "index_service_points_on_user_id"

  create_table "tokens", :force => true do |t|
    t.string   "token"
    t.string   "last_four"
    t.string   "exp_y"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "exp_m"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false
    t.boolean  "profile_complete",       :default => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "zones", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
