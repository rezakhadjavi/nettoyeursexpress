class CreateBillings < ActiveRecord::Migration
  def change
    create_table :billings do |t|
      t.string :method
      t.boolean :primary
      t.integer :user_id
      t.integer :token_id

      t.timestamps
    end
  end
end
