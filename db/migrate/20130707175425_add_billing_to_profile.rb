class AddBillingToProfile < ActiveRecord::Migration
  def self.up
    add_column :profiles, :billing, :string
  end

  def self.down
    add_column :profiles, :billing
  end
end
