class AddMonthToExp < ActiveRecord::Migration
  def change
    add_column :tokens, :exp_m, :string
    rename_column :tokens, :exp_date, :exp_y
  end
end
