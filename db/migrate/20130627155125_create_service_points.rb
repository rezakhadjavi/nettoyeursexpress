class CreateServicePoints < ActiveRecord::Migration
  def change
    create_table :service_points do |t|
      t.string :address
      t.string :apt
      t.string :door_code
      t.string :postal_code
      t.string :title
      t.string :instructions
      t.integer :user_id

      t.timestamps
    end
  end
end
