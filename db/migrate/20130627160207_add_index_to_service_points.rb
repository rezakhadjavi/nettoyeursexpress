class AddIndexToServicePoints < ActiveRecord::Migration
  def change
    add_index :service_points, :user_id
  end
end
