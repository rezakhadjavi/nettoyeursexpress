class AddCNotesToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :c_notes, :string
  end

  def self.down
    add_column :orders, :c_notes
  end
end
