class RemoveBillingFromProfile < ActiveRecord::Migration
  def change
    remove_column :profiles, :billing
  end
end
