class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.integer :phone
      t.integer :ext
      t.integer :user_id

      t.timestamps
    end
    add_index :profiles, [:phone, :user_id]
  end
end
