class AddProfileCompleteToUser < ActiveRecord::Migration
   def self.up
    add_column :users, :profile_complete, :boolean, default: false
  end

  def self.down
    remove_column :users, :profile_complete
  end
end
