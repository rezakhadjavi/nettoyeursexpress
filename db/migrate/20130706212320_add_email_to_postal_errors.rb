class AddEmailToPostalErrors < ActiveRecord::Migration
  def self.up
    add_column :postal_errors, :email, :string
    add_column :postal_errors, :name, :string
  end

  def self.down
    add_column :postal_errors, :email
    add_column :postal_errors, :name
  end
end
