class CreatePostalErrors < ActiveRecord::Migration
  def change
    create_table :postal_errors do |t|
      t.string :postal_code

      t.timestamps
    end
  end
end
