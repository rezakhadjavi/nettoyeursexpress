class ChangeExtFormatInProfiles < ActiveRecord::Migration
  def up
    change_column :profiles, :ext, :string
  end

  def down
    change_column :profiles, :ext, :integer
  end
end
