class AddZoneIdToServicePoints < ActiveRecord::Migration
   def self.up
    add_column :service_points, :zone_id, :integer
  end

  def self.down
    remove_column :service_points, :zone_id
  end
end
