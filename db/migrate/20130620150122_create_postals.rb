class CreatePostals < ActiveRecord::Migration
  def change
    create_table :postals do |t|
      t.string :postal_code
      t.string :zone_id

      t.timestamps
    end
    
    add_index :postals, :zone_id
  end
end
