class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.date :p_date
      t.string :p_time
      t.date :d_date
      t.string :d_time
      t.string :p_address
      t.string :d_address
      t.string :p_postal
      t.string :d_postal
      t.string :p_notes
      t.string :d_notes
      t.string :service
      t.string :speed
      t.string :promo_code
      t.string :billing
      t.string :status
      t.string :total
      t.integer :invoice_id
      t.integer :user_id

      t.timestamps
    end
  end
end
