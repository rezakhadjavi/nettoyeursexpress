class AddCellToProfiles < ActiveRecord::Migration
  def self.up
    add_column :profiles, :cell, :string
  end

  def self.down
    remove_column :profiles, :cell
  end
end
