class RemoveProfileFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :name
    remove_column :users, :phone
    remove_column :users, :ext
  end
end
